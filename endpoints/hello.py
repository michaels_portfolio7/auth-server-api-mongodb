# hello.py script
from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
import models

from database import engine, SessionLocal


router = APIRouter()

def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@router.get("")
async def say_hi(name: str):
    return "Hi "


@router.get('/users/all', tags=["Authentication"])
def all_users(db: Session = Depends(get_db)):
    return db.query(models.UserInfo).all()
