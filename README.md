# Python API (FastAPI/MySQL)

This is a authentication and tokenized login and signup API with email verification codes, security tokens and browser session storage. 

Here is a video live **[DEMO LINK](https://drive.google.com/file/d/1itWyEWEib2eCntc5X759kSxBsL0NHd6Z/preview)**.


