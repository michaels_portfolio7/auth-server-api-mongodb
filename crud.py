from sqlalchemy.orm import Session
import bcrypt
from schemas import schema_user
import models
import secrets
from postmarker.core import PostmarkClient

postmark = PostmarkClient(server_token='') #REMOVED TOKEN

##### USERS #######
def get_user_by_username(db: Session, username: str):
    return db.query(models.UserInfo).filter(models.UserInfo.username == username).first()

### get user by role
def get_user_by_role(db: Session, username:str, role:str):
    # rolequery = db.query(models.UserInfo.username == username).filter(
    #     models.UserInfo.role == role
    # )
    query = db.query(models.UserInfo).filter(models.UserInfo.username == username, models.UserInfo.role == role)
    result = query.first()

    return result


def create_user(db: Session, user: schema_user.UserCreate):
    hashed_password = bcrypt.hashpw(user.password.encode('utf-8'), bcrypt.gensalt())
    secure_token = secrets.token_urlsafe(32)
    db_user = models.UserInfo(username=user.username, password=hashed_password, email=user.email, phone=user.phone, idtoken=secure_token, role='user')
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    variable= f"Hello Neo a.k.a {user.username}, This is your token: {secure_token} Follow the white rabbit."
    postmark.emails.send(
        From='yes@michaelbytes.com',
        To='yes@michaelbytes.com',
        Subject='New User: Your Token',
        HtmlBody=variable
    )
    return db_user

def token_email(db: Session, token: schema_user.UserCreate):
    pass

def check_username_password(db: Session, user: schema_user.UserAuthenticate):
    db_user_info: models.UserInfo = get_user_by_username(db, username=user.username)
    return bcrypt.checkpw(user.password.encode('utf-8'), db_user_info.password.encode('utf-8'))

def check_username_token(db: Session, user: schema_user.UserAuthenticate):
    db_userinfo: models.UserInfo = get_user_by_username(db, username=user.username)
    return db_userinfo.idtoken == user.idtoken

def forgot_password(db: Session, username: str):
    #make new temp password
    password = secrets.token_urlsafe(8)
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
    #Get user from username
    query = db.query(models.UserInfo).filter(models.UserInfo.username == username).update(dict(password=hashed_password))
    #save to DB
    db.commit()

    #send email with temp password
    variable= f"Hello Neo a.k.a {username}, This is your new temporary password: {password}. Please login to now to change... URL"
    postmark.emails.send(
        From='email@email.com',
        To='email@email.com',
        Subject='Your Temporary Password',
        HtmlBody=variable
    )

