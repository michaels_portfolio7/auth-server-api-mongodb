from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Date, Float

from database import Base



class UserInfo(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    password = Column(String)
    email = Column(String)
    phone = Column(Integer)
    idtoken = Column(String)
    role = Column(String)

