import uvicorn
from sqlalchemy.orm import Session
from fastapi import Depends, FastAPI, HTTPException, File, UploadFile
import models

from database import engine, SessionLocal
from schemas import schema_user
import crud
from postmarker.core import PostmarkClient
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm


models.Base.metadata.create_all(bind=engine)


ACCESS_TOKEN_EXPIRE_MINUTES = 30



postmark = PostmarkClient(server_token='126ea624-xxxxx') #REMOVED TOKEN

app = FastAPI(
    title='Kassandra API',
    description='Oh my.'
)

# routes
from endpoints import hello
from app_utils import has_access

PROTECTED = [Depends(has_access)]

app.include_router(
    hello.router,
    prefix="/hello",
    dependencies=PROTECTED
)

from endpoints import admin
from app_utils import has_admin_access

ADNINPROTECTED = [Depends(has_admin_access)]
app.include_router(
    admin.router,
    prefix="/admin",
    dependencies=ADNINPROTECTED
)

# Cross Browser

from fastapi.middleware.cors import CORSMiddleware
origins = [
"http://localhost",
"http://localhost:3000",
]
app.add_middleware(
CORSMiddleware,
allow_origins=origins,
allow_credentials=True,
allow_methods=["*"],
allow_headers=["*"],
)

# Dependency


def get_db():
    db = None
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()




@app.post("/api/token", response_model=schema_user.Token)
def login_for_access_token(db: Session = Depends(get_db),
                      	form_data: OAuth2PasswordRequestForm = Depends()):

   """generate access token for valid credentials"""
   user = authenticate_user(db, form_data.username, form_data.password)
   if not user:
   	raise HTTPException(
       	status_code=HTTP_401_UNAUTHORIZED,
       	detail="Incorrect email or password",
       	headers={"WWW-Authenticate": "Bearer"},
   	)
   access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
   access_token = create_access_token(data={"sub": user.email},
                                  	expires_delta=access_token_expires)
   return {"access_token": access_token, "token_type": "bearer"}


#### USER

@app.get('/users/all', tags=["Authentication"])
def all_users(db: Session = Depends(get_db)):
    return db.query(models.UserInfo).all()


@app.post("/user", response_model=schema_user.UserInfo, tags=["Authentication"])
def create_user(user: schema_user.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")
    # emailtoken = crud.check_username_token(db, db_user.username)
    # postmark.emails.send(
    #     From='yes@michaelbytes.com',
    #     To='yes@michaelbytes.com',
    #     Subject='New User',
    #     HtmlBody= 'HTML body goes here'%emailtoken
    # )
    return crud.create_user(db=db, user=user)

@app.get("/user/",  response_model=schema_user.UserInfoBase, tags=["Authentication"])
def get_data(user: schema_user.UserInfoBase, db: Session = Depends(get_db)):
    return get_user_by_username(db=db, user=user.username)

@app.put("/user", tags=["Authentication"])
def update_data():
    return ('test:test')

@app.delete("/user", tags=["Authentication"])
def delete_data():
    return ('test:test')

@app.get('/login', tags=["Authentication"])
def user():
    return {'user'}

@app.put('/forgotpassword', response_model=schema_user.UserForgotPass,tags=["Authentication"])
def forgot_password(user: schema_user.UserForgotPass, db: Session = Depends(get_db)):
    return crud.forgot_password(db=db, username=user.username)
    # return {'user'}

@app.get('/tokenreset', tags=["Authentication"])
def token_reset():
    return {'user'}


@app.post("/authenticate", response_model=schema_user.Token, tags=["Authentication"])
def authenticate_user(user: schema_user.UserAuthenticate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user is None:
        raise HTTPException(status_code=400, detail="Username not existed")
    else:
        is_password_correct = crud.check_username_password(db, user)
        if is_password_correct is False:
            raise HTTPException(status_code=400, detail="Password is not correct")
       #
        else:
            token_user = crud.check_username_token(db, user)
            if token_user is False:
                raise HTTPException(status_code=400, detail="Token is not correct")
            #
            else:
                from datetime import timedelta
                access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
                from app_utils import create_access_token
                access_token = create_access_token(
                    data={"sub": user.username}, expires_delta=access_token_expires)
                return {"access_token": access_token, "token_type": "Bearer"}


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=7777)
