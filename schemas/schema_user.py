from pydantic import BaseModel



class UserInfoBase(BaseModel):
    username: str
    password: str
    # idtoken: str


class UserCreate(UserInfoBase):
    password: str
    email: str
    phone: int



class UserAuthenticate(UserInfoBase):
    password: str
    idtoken: str

class UserForgotPass(BaseModel):
    username: str

class UserForgotPassReset(UserForgotPass):
    email: str
    username: str
    password: str

class UserInfo(UserInfoBase):
    id: int

    class Config:
        orm_mode = True

#############


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str = None

